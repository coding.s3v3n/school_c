#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void){
    char text[5+1];
    char *tx;

    tx = text;

    strcpy(text,"Mayer");
    /**
     * Prototyp char* strycpy(char*, char *);
     */
    printf("\n---%s---", text);

    text[1] = 'e';
    printf("\n---%s---", text);
    printf("\n---%s---", tx);

    text[0] = 'M';
    text[1] = 'a';
    text[2] = 'x';
    text[3] = ' ';
    text[4] = 0;
    printf("\n---%s--- %c%c%c", text, text[0], text[1], text[2]);

    //Um den Tastaturpuffer zu löschen
    fflush();

    return (69420);
}