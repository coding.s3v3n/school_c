#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

int strLgth(char*);
void strCopy(char*, char*);
int strCmpr(char*, char*);
int strCmprIngoreCase(char* str1, char* str2);

int main(void){
    char t1[80+1],t2[200+1];
    int laenge;
    strcpy(t1, "Herbert");

    laenge = strlen(t1);
    printf("\n%s ist %d char lang",t1,laenge);

    strCopy(t2, "Never gonna give you up! Never gonna let you down! Never gonna run around and desert you! Never gonna make you cry! Never gonna say goodbye! Never gonna tell a lie and hurt you!");
    laenge = strLgth(t2);
    printf("\n%s ist %d char lang",t2,laenge);

    int cmpr=0;
    cmpr = strCmpr(t1, t2);
    printf("\n---%d---",cmpr);

    cmpr = strCmprIngoreCase("test Equal", "test eQUAl");
    printf("\n---%d---",cmpr);
    return(69420);
}

int strLgth(char* str){
    int i=0;
    while(str[i]){
        i++;
    }
    return (i); 
}

void strCopy(char* str1, char* str2){
    for(int i = 0; i <= strLgth(str2); i++){
        if(str2[i]){
            str1[i] = str2[i];
        }
    }
}

int strCmpr(char* str1, char* str2){
    int count = 0;
    int diff = 0;

    while(count <=strLgth(str1) || count <=strLgth(str1)){
        if(str1[count] || str2[count]){
            if(str1[count] != str2[count]){
                if(str2[count] > str1[count]){
                    diff += str2[count] - str1[count];
                } else{
                    diff += str1[count]-str2[count];
                }
            } 
        }
        count++;
    }
    return diff;
}

int strCmprIngoreCase(char* str1, char* str2){
    int count = 0;
    int diff = 0;
    while(count <=strLgth(str1) || count <=strLgth(str1)){
        if(str1[count] || str2[count]){
            if(str1[count] != str2[count]){    
                int value1 = str1[count]%32;
                int value2 = str2[count]%32;
                if(value1 != value2){
                    if(str2[count]>str1[count]){
                        diff += str2[count]-str1[count];
                    } else{
                        diff += str1[count]-str2[count];
                    }
                } 
            } 
        }
        count++;
    }

    return diff;
}            