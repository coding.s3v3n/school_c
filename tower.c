#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

void algotrith(int, int, int);

int main(void){
    int pieces = 3, start = 1, goal = 3;
    
    algotrith(pieces, start, goal);

    printf("\n");
    printf("\n");
    return(69420);
}

void algotrith(int piece, int from, int to){
    int between;
    if(piece>1){
        between = 6-from-to;
        algotrith(piece-1, from, between);
        printf("\nScheibe %2d verschoben von %2d nach %2d", piece, from, to);
        algotrith(piece-1,between,to);
    } else{
        printf("\nScheibe %2d verschoben von %2d nach %2d", piece, from, to);
    }
}