#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main(void){
    char input[9+1];
    
    scanf("%s", input);
    fflush(stdin);
    int currPart = 0;
    int currPos = 0;
    char parts[3][3+1];
    char part[3+1];
    char a[1+1];

    //Input string aufteilen
    for (int i = 0; i <= sizeof input; i++)
    {
        if(input[i] == '-'){
            currPart++;
            currPos=0;
        }else{
            a[0] = input[i];
            a[1] = 0x0;
            strcat(parts[currPart], a);
        }        
    }
    
    //prüfsumme berechnen
    int counter = 1;
    int pruefsumme = 0;
    for(int i = 0; i < 2; i++){
        for(int j=0; j < sizeof parts[0]-1; j++){
        if(counter%2==0){
            pruefsumme += getQuersumme((parts[i][j]-48)*2);
        }else{
            pruefsumme += getQuersumme((parts[i][j]-48));
        }
        counter++;
        }
    }
    //
    char pruef[2+1];
    pruef[0]= '-';
    pruef[1]= getPruefsumme(pruefsumme)+48;
    pruef[2]= 0X0;
    strcpy(parts[2], pruef);
    strcat(input, pruef);
    printf("\n---%s---",input);
    printf("\n");
    return(69420);
}

int getQuersumme(int value){
    if(value==-48){
        value +=48;
        }
    int first = value/10;
    int second = value-(first*10);
    return (first+second);
}

int getPruefsumme(int value){
    printf("\n---%d---",value);
    int a = value % 10;
    printf("\n---%d---",a);

    if(a==0){
        a = 10;
    }
    return (10-a);
}