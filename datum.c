#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>

#define stringify( name ) # name

void convDateToString(char*);

int main(void){
    char date[80+1];
    
    convDateToString(date);
    
    printf("\n%s",date);
    return(69420);
}

/**
 * @brief 
 * int  tm_sec;  	 Seconds: 0-60 (to accommodate leap seconds) 
 * int  tm_min;  	 Minutes: 0-59 
 * int  tm_hour; 	Hours since midnight: 0-23 
 * int  tm_mday; 	 Day of the month: 1-31 
 * int  tm_mon;  	 Months *since* January: 0-11 
 * int  tm_year; 	 Years since 1900 
 * int  tm_wday; 	 Days since Sunday (0-6) 
 * int  tm_yday; 	Days since Jan. 1: 0-365 
 * int  tm_isdst;	+1=Daylight Savings Time, 0=No DST, -1=unknown 
 * @param datum 
 */

void convDateToString(char* date){
    char htext[20+1];
    struct tm *zeit; // jwd-Zeiger einer tm-Struktur angelegt
    long sek; // Datentyp time_t gleichbedeutend mit long
    //! Verusch den Tag und Monat mit einem Enum anzugeben
    enum month{Januar=1, Februar, Maerz, April, Mai, Juni, Juli, August, September, Oktober, November, Dezember};
    enum week{Sonntag=0, Montag, Dienstag, Mittwoch, Donnerstag, Freitag, Samstag};

    time(&sek); // die Variable sek wird mit Anzahl Sekunden set 1.1.1970 gefüllt
            
    zeit = localtime(&sek);
    switch(zeit->tm_wday){
        case 0:
        strcpy(date, "Sonntag, den ");
        break;
        case 1:
        strcpy(date, "Montag, den ");
        break;
        case 2:
        strcpy(date, "Dienstag, den ");
        break;
        case 3:
        strcpy(date, "Mittwoch, den ");
        break;
        case 4:
        strcpy(date, "Donnerstag, den ");
        break;
        case 5:
        strcpy(date, "Freitag, den ");
        break;
        case 6:
        strcpy(date, "Samstag, den ");
        break;
        default:
        printf("Programmfehler in zeit->tm_wday");
    }
    
    char* placeholder;
    sprintf(placeholder, "%d.",zeit->tm_mday);
    strcat(date, placeholder);
    printf("\n---%c---",stringify(Januar) );
    sprintf(placeholder, "%d",zeit->tm_year+1900);
    strcat(date, placeholder);
}