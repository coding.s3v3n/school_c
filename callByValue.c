#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

void up_addiere(int *a, int *b, int *erg);

int main(void){
    int a=4,b=7,erg;
    up_addiere(&a, &b, &erg);
    printf("\n---%d---",erg);
    return(69420);
}

void up_addiere(int *a, int *b, int *erg){
    *erg = *a+*b;
}
