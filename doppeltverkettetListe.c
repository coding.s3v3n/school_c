#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

typedef struct m_dhler{
    char name[20+1];
    float gehalt;
    struct m_dhler *davor;
    struct m_dhler *dannach;
} t_dhler;

typedef struct{
    char name[20+1];
    float gehalt;
    t_dhler *momentan;
    t_dhler *start;
    t_dhler *zwischen;   
}t_feld;

void list_add(t_feld*);
void struct_liste(t_feld*);
void hexterminate(t_feld*);

int main(void){
    t_feld feld, *f = &feld;

    f->start = 0;
    f->momentan = 0;
    f->zwischen = 0;

    for(int i = 0; i<4;i++){
        printf("\nGib den %d. Name ein ",i+1);
        fgets(f->name, 21, stdin);
        f->name[strcspn(f->name, "\n")] = 0;
        liste_add(f);
    }
    hexterminate(f);
    printf("\n");
    return(69420);
}

void liste_add(t_feld *f){
    f->momentan = (t_dhler*) malloc(sizeof(t_dhler));
    struct_liste(f);
    f->momentan->davor = f->zwischen;
    f->momentan->dannach = 0;
    //If zwischen != 0
    //Nur für den ersten fall
    if(!f->zwischen){
        f->start = f->momentan;
    } else{
        f->zwischen->dannach = f->momentan;
    }
    f->zwischen = f->momentan;
}

void struct_liste(t_feld *f){
    //-> greife über zeiger auf die Variable zu 
    strcpy(f->momentan->name, f->name);
}

void hexterminate(t_feld *f){
    f->momentan = f->start;
    while(f->momentan){
        printf("\n%-10s die Bitch, Schuldet mir Geld",f->momentan->name);
        printf("\nMein: %X",f->momentan);
        printf("\nDavor:%X ",f->momentan->davor);
        printf("\nDannach: %X",f->momentan->dannach);
        f->momentan = f->momentan->dannach;
    }  
};

//free(f->momentan) mach den Speicherplatz frei