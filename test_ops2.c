#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void){
    int a = 7,  b = 4, erg;
    
    erg = a / b; // erg = 1
    erg = a % b; // erg = 3

    a = 7;
    erg = a>>2; //0000....0111  -->  0000....0001 (Schiebe um 2 bit nach rechts)
        //gleichzusetzen mit 7 / 2^2 == 1
    printf("\n %d", erg);

    a = 7;
    erg = a<<3 ; //0000....0111  -->  0000....0011 1000
        //gleichzusetzen mit 7*2^3 == 56
    printf("\n %d", erg);
    
    if(a>56){

    }else{

    }

    //dies ist KEIN Vergleich sondern eine Zuweisung !!!!!!!!
    //Dauerhaft Wahr weil a das Bitmuster von 4 enthält und somit != null ist
    if(a=4){
        printf("\n wahr");
    }else{
        printf("\n falsch");
    }

    a = 7, b = 13;
    //Bitweise darstellung
    //  7   0111   
    //  13  1101
    //  &   0101    ==  5
    //  |   1111    ==  15
    //  ^   1010    ==  10
    erg = a & b; 
    printf("\n a & b = %d", erg);

    erg = a | b;
    printf("\n a | b = %d", erg);

    erg = a ^ b;
    printf("\n a ^ b = %d", erg);

    int schalter, venti=0X20, licht=0X40;
                //0000....0010     0000....0100
    
    schalter = licht; 
    //outp(schalter);
    schalter = venti;
    //outp(schalter);

    schalter = licht | venti;
    //outp(schalter);    

    /*
    if((a>b && c==4) || x+y < 56){

    } else{

    }
    */

   //a>36 ?: x=7 : Y = Pi+paddelboot;
   /*
    if(a>35){
        x=7;
    } else{
        Y = Pi+paddelboot;
    }
    */

   return(69420);
}