//"" weil die Header file im Ordner liegt
#include "bahn.h"

//In Dateien schreiben, aus Dateien lesen, neue Elemente erstellen usw.
/**
 * @brief 
 * 1. Lese Datei Bahn.t
    In Dateien schreiben, aus Dateien lesen, neue Elemente erstellen usw.(Ideal Json Format)
 * 
 * @return int 
 */


void list_add(T_feld*);
void struct_liste(T_feld*);
void up_einlesen_Tastatur(T_feld *f);
void showallElements(T_feld *f);
void addToField(char* value, T_feld *f);
void remove_spaces(char* s);

int main(void){

    //Methode um das Menu aufzurufen
    T_feld *f; 
    f->start = 0;
    f->momentan = 0;
    f->zwischen = 0;
    int auswahl = 1;

    while(auswahl){        
        printf("\nWähle eine Aktion aus: ");
        fflush(stdin);
        scanf("%d",&auswahl);
        fflush(stdin);
        printf("\n");
        while(auswahl>10 || auswahl<0){        
            printf("\nWähle eine Aktion aus: ");
            fflush(stdin);
            scanf("%d",&auswahl);
            fflush(stdin);
            printf("\n");
        }
        switch(auswahl){
            case 1:
                //Einlesen der Datei keine glob Variable 
                readfile(f);
            break;
            case 2:
                up_einlesen_Tastatur(f);
                //Einlesen von Tastatur input(heißt neues Element) up_einlesen_Tastatur(f)
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            case 7:
            break;
            case 8:
            break;
            case 9:
            break;
            case 10:
            break;
            case 0:
            break;
            default:
                printf("Falsche Eingabe");
        }
    }

    showallElements(f);
    printf("\n");
    return(69420);
}


void up_einlesen_Tastatur(T_feld *f){
    printf("Gib den Namen der Bahn ein: ");
    scanf("%s",f->name);
    printf("\n");

    printf("Gib die Seriennummer der Bahn ein: ");
    int test;
    scanf("%d",&test);
    f->serial_nr = test;
    printf("\n");

    printf("Gib den Typen der Bahn ein: ");
    scanf("%s",f->type);
    printf("\n");
    liste_add(f);
}

void liste_add(T_feld *f){
    f->momentan = (T_bahn*) malloc(sizeof(T_bahn));
    struct_liste(f);
    f->momentan->davor = f->zwischen;
    f->momentan->dannach = 0;
    //If zwischen != 0
    //Nur für den ersten fall
    if(!f->zwischen){
        f->start = f->momentan;
    } else{
        f->zwischen->dannach = f->momentan;
    }
    f->zwischen = f->momentan;
}


void struct_liste(T_feld *f){
    //-> greife über zeiger auf die Variable zu 
    strcpy(f->momentan->name, f->name);
    f->momentan->serial_nr = f->serial_nr;
    strcpy(f->momentan->type, f->type);

}

void showallElements(T_feld *f){
    f->momentan = f->start;
    while(f->momentan){
        printf("\nName: %-30s, Seriennummer: %10.0d, Type: %-20s",f->momentan->name, f->momentan->serial_nr, f->momentan->type);
        f->momentan = f->momentan->dannach;
    }
}

void readfile(T_feld *f){
    FILE *file;
    file = fopen("bahn.txt","a+");
    char buffer[80+1];
        if(!file){
            printf("Datei kann nicht geöffnet werden");
        } else{
            while(!feof(file)){
                fgets(buffer, 80, file);
                buffer[strcspn(buffer, "\n")] = 0;
                remove_spaces(buffer);
                addToField(buffer,f);
                //printf("\n---%s---",buffer);
            }
        }

    fclose(file);
}

void remove_spaces(char* s) {
    char* d = s;
    do {
        while (*d == ' ') {
            ++d;
        }
    } while (*s++ = *d++);
}

void addToField(char* buffer, T_feld *f){
    char* type;
    char* value;
    type = malloc(1*sizeof(char));

    strcpy(value, buffer);
    value += strcspn(buffer, ":")+1;
    value[strcspn(value, ",")] = 0;
    type[strcspn(type, ",")] = 0;
    
    for(int i =0; i <strcspn(buffer, ":"); i++){
        type = realloc(type, (i+1)*sizeof(char));
        type[i]=buffer[i];
    }
    //printf("\n---%s   %s   %d---",type, value ,strcspn(value, ","));
    if(strcmp(type,"name")==0){
        strcpy(f->name, value);
    } else if(strcmp(type,"serial_nr")==0){
        f->serial_nr = myAtoi(value);
    } else if(strcmp(type,"type")==0){
        strcpy(f->type, value);
    } else if(strcmp(type,"}")==0||strcmp(type,"},")==0){
        liste_add(f);
    }
}

int myAtoi(char* a){
    return 0;
}