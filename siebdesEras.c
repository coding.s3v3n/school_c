#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void countPrimes(int *count, int upper);
void nimmzeit(double *messwert);

int main(void){
    int count=0;
    countPrimes(&count, 1000000);
    printf("\n---%d---",count);
    printf("\n");
    return(69420);
}

void countPrimes(int *count, int upper){
    bool isPrime = true;
    int *primes;
    primes = malloc(1);
    for(int i = 2; i<upper; i++) {
        int limit = sqrt(i);
        for(int j = 0; j<=(*count)-1;j++) {
            if(primes[j]<=limit){
                if(i%primes[j]==0){
                    isPrime = false;
                    break;
                }
            }
        }
        if(isPrime){
            primes = (int*)realloc(primes,((*count)+1)*sizeof(int));
            primes[*count]=i;
            (*count)++;
        } else{
            isPrime = true;
        }
    }
}