#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void){

    char name[50+1];
    char t1[100+1];
    
    /**
     * @brief Stringfunktion Copy
     * * Funktionsprototyp  char* strcpy(char *, const char * )
     * Zwei zeiger einmal auf das Array, einmal auf den Wert
     * @return Returnt ein Zeiger(Array)
     */
    strcpy(name, "Hannes");
    strcpy(t1, name);//! Arrays sind Zeiger

    /**
     * @brief Stringfunktion Copy mit Länge
     * * Funktionsprototyp char* strcpy(char *, const char *, size_t n)
     * Zwei zeiger einmal auf das Array, einmal auf den Wert, Beginnt bei der Hex0, die Hex0 wird nicht in den neuen Wert kopiert
     * @return Returnt ein Zeiger(Array)
     */
    strncpy(t1, name,10);
    strncpy(t1,name+7,3);//copiert ab dem 7ten bit die nächsten drei in t1

    /**
     * @brief Stringfunktion Append
     * * Funktionsprototyp char* strcat(char *, const char * )
     * Zwei zeiger einmal auf das Array, einmal auf den Wert, Beginnt bei der Hex0
     * @return Returnt ein Zeiger(Array)
     */
    strcat(name, " ist ein Echsenmensch!!!!");
    strcat(t1, name); //! Arrays sind Zeiger

    /**
     * @brief Stringfunktion Reverse
     * * Funktionsprototyp char* strrev(char*)
     * Reversed die Vorherige Aktion des Zeigers (Aus "Hannes ist ein Echsenmensch" wird "Hannes")
     * @return Returnt ein Zeiger(Array)
     */
    strrev(t1);

    /**
     * @brief Stringfunktion Compare 
     * * Funktionsprototyp int strcmp(const char *, const char * )
     * Vergleicht 2 Strings, indem der zeite string vom ersten char für char abgezogen wird
     * @example t1 = "Hugo", t2 = "Hugo", t3="Hugo", t4="Hugo lebt"
     * strcmp(t1,t2) = 0
     * strcmp(t1,t2) != 0
     * !Von einem String wird der andere Abgezogen, wenn sie Gleich sind, dann ist @return 0, sonst ungleich 0
     * @return 0, -1 oder 1
     */
    strcmp(t1, name);

    /**
     * @brief Stringfunktion Compare mit Länge
     * * Funktionsprototyp int strncmp(const char *, const char * , size_t n)
     * Vergleicht 2 Strings, indem der zeite string vom ersten char für char abgezogen wird
     * @example t1 = "Hugo", t2 = "Hugo", t3="Hugo", t4="Hugo lebt"
     * strncmp(t1,t2) = 0
     * strncmp(t1,t3,2) = 0
     * !Von einem String wird der andere Abgezogen, wenn sie Gleich sind, dann ist @return 0, sonst ungleich 0
     * @return 0, -1 oder 1
     */
    strncmp(t1,name, 4);

    /**
     * @brief Stringfunktion Compare Case Ignore
     * * Funktionsprototyp int strcasecmp(const char *, const char * )
     * Vergleicht 2 Strings, indem der zeite string vom ersten char für char abgezogen wird
     * ! Ignoriert Case sensitity
     * @example t1 = "Hugo", t2 = "Hugo", t3="Hugo", t4="Hugo lebt"
     * strcasecmp(t1,t2) = 0
     * strcasecmp(t1,t3) = 0
     * !Von einem String wird der andere Abgezogen, wenn sie Gleich sind, dann ist @return 0, sonst ungleich 0
     * @return 0, -1 oder 1
     */
    strcasecmp(t1,name);

    /**
     * @brief 
     * * Funktionsprototyp char* strchr(char*, int)
     * Gib das erste Vorkommen des characters 
     * !Bezieht sich dann auf das erste char
     * @return Returnt ein Zeiger(Array)
     */
    strchr(t1, 'a');
    char bu = 'n';
    char text[80+1];
    char *z;
    strcpy(text, "Hannes");
    z = strchr(text, bu);

    /**
     * @brief 
     * * Funktionsprototyp char* strstr(char*, char*)
     * Gib das letzte Vorkommen des characters 
     * @return Returnt ein Zeiger(Array)
     */
    strstr(t1,name);

    return(69420);
}