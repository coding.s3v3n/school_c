#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}
int main(void){
    int pruef = 0;
    char kontonummer[20+1];
    char pruefsu[2];
    char ursprungsnummer[20+1];

    scanf("%s", ursprungsnummer);
    fflush(stdin);
    strrev(ursprungsnummer);

    for(int i = 0; i < sizeof ursprungsnummer; i++){
        int a = ursprungsnummer[i]-48;
        if(0<=a && a<=9){
            int side = i/6;
            int multiplier = 2+i;
            multiplier = multiplier-(side*6);
            pruef += a*multiplier;
        }
    }
    
    pruef =(pruef%11);
    pruef = 11-pruef;
    strcpy(kontonummer,ursprungsnummer);

    pruefsu[0] = pruef+48;
    pruefsu[1] = 0;
    strcat(kontonummer, pruefsu);

    printf("\n---%s---",kontonummer);
    
    printf("\n");

    return(69420);
}
