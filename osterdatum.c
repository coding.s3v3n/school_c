#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void) {
    int jahr = 1900, a, b, c, d, e, tag, anzZeile = 0;

    printf("\n");
    while(jahr<=2025){
        
        if(jahr==1954 || jahr==1981){
            printf("\033[1;31m");
            printf("%4.d ist Ostern nicht berechenbar \n", jahr);
            printf("\033[1;0m");
        }else{
            a = jahr%19;
            b = jahr%4;
            c = jahr%7;
            d = ((19*a)+24)%30;
            e = ((2*b)+(4*c)+(6*d)+5)%7;
            tag = d+e+22;
            if(tag>31){
                //April
                printf("%4.d ist Ostern am %2.d.April!\n", jahr, tag-31);
            } else{
                //März
                printf("%4.d ist Ostern am %2.d.Maerz!\n", jahr, tag);
            }
        }
        jahr++;
        if(anzZeile==25){
            anzZeile = 0;
            getch();
        }
        anzZeile++;
    }
    return (69420);
}