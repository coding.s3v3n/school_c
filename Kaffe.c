#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>

char names[2][15+1]; 
int upper = 1000;
int lower = 0;
int guess = -1;
int turn = 0;

void setPlayername();
void guessANumberInSpan();
void readInputName(int position);

int main(void){
    srand(time(0));
    int randoBitch = rand()%upper;
    printf("\n");
    setPlayername(); 
    while(true){
        while(guess>upper || guess< lower){
            guessANumberInSpan(turn);
            if(guess>upper || guess< lower){
                printf("\n%s your guess(",names[turn]);
                printf("\033[1;31m");
                printf("%d", guess);
                printf("\033[1;0m");
                printf(") is out of bound");
            }
        }
        if(guess==randoBitch){
            printf("\n%s Hat die Tasse Kaffe gewonnen",names[turn]);
            printf("\n%s Muss den Kaffe bezahlen",names[!turn]);
            break; 
        }else { 
            if(guess>randoBitch){
                upper = guess;
            } else if(guess<randoBitch){
                lower = guess;
            }
        }
        //Count up to turns and get the modulo for the Playercount
        turn++;
        turn = turn % ((sizeof names)/(sizeof names[0]));
        guess = -1;
    }

    printf("\n");

    return(69420);
}   

void setPlayername(){
    for (size_t i = 0; i < (sizeof names)/(sizeof names[0]) ; i++)
    {
        readInputName(i);
    }
}

void readInputName(int position){
    char name[15+1];
    printf("\nWelcome Player please state your name: ");
    fgets(name, 16, stdin);
    fflush(stdin);
    name[strcspn(name, "\n")] = 0X0;
    strcpy(names[position], name);
    printf("\nThank you %s",names[position]);
}

void guessANumberInSpan(){ 
    printf("\n%s guess a number between %d and %d: ",names[turn], lower, upper);
    scanf("%d", &guess);
}