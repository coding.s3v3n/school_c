#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

int real_fakul(int);
int test_fakul(int);

int main(void){
    int testingBitches = 4;
    printf("\n---test_fakul: %d---",test_fakul(testingBitches));
    printf("\n---real_fakul: %d---",real_fakul(testingBitches));
    printf("\n");
    return(69420);
}

int real_fakul(int n){
    int res = 1;
    for(int i = 1; i<=n; i++){
        res *= i;
    }
    return res;
}

//Rekursion mit der Funktion test_fakul
int test_fakul(int n){
    int res = 0;
    if(n>1){
        res = n*test_fakul(n-1);
    } else{
        res = 1;
    }
    return res;
}