#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

void algorith(int n);

int main(void){
    algorith(18);
    printf("\n");
    return(69420);
}

void algorith(int n){
    for(int i = 1; i<=n;i++){
        printf("\n%6s%2.0d%4s%6.0f%6s%1.18f","",i,"",pow(2,i),"",1/pow(2,i));
    }
}
